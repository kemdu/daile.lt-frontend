var gulp = require('gulp'),
    sass = require('gulp-sass'),
    bower = require('gulp-bower'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    min_css = require('gulp-minify-css'),
    rename = require('gulp-rename'),
    sourcemaps = require('gulp-sourcemaps'),
    notify = require('gulp-notify');

gulp.task('init', function() {
    bower();
    gulp.src([
        'bower_components/jquery/dist/jquery.min.js',
        'bower_components/jquery/dist/jquery.min.map',
        'bower_components/bootstrap/dist/js/bootstrap.min.js'
    ])
        .pipe(gulp.dest('js'));

    gulp.src([
        'bower_components/bootstrap-sass-official/assets/fonts/*'
    ])
        .pipe(gulp.dest('fonts'));

    gulp.src([
        'bower_components/bootstrap-sass-official/assets/stylesheets/*'
    ])
        .pipe(gulp.dest('sass/bootstrap'));

    gulp.src([
        'bower_components/bootstrap-sass-official/assets/stylesheets/bootstrap/*'
    ])
        .pipe(gulp.dest('sass/bootstrap/bootstrap'));

    gulp.src([
        'bower_components/bootstrap-sass-official/assets/stylesheets/bootstrap/mixins/*'
    ])
        .pipe(gulp.dest('sass/bootstrap/bootstrap/mixins'));
});

gulp.task('watch', function () {
    gulp.watch('sass/*', ['sass']);
    gulp.watch(['js/*.js','!js/all.js'], ['js']);
});

gulp.task('sass', function () {
    gulp.src('sass/daile.scss')
        .pipe(sass())
        .pipe(gulp.dest('css'))
        .pipe(min_css())
        .pipe(rename('daile.min.css'))
        .pipe(gulp.dest('css'));
});

gulp.task('js', function() {
    gulp.src(
        [
            'js/jquery-1.9.1.js',
            'js/jquery-ui-1.9.2.js',
            'js/jquery.shapeshift.js',
            'js/jquery.zoom.js',
            'js/scripts.js'
        ])
        .pipe(sourcemaps.init())
        .pipe(concat('all.js'))
        .pipe(uglify('all.js'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('js'));
});

gulp.task('default', ['js','sass', 'watch']);