$.fn.items_hover = function() {
    $('a.item').each(function() {
        var item_height = $(this).find('.item-info').outerHeight();
        $(this).css('padding-bottom',item_height);
    });
    $('a.author-item').each(function() {
        var item_height = $(this).find('.author-item-info').outerHeight();
        $(this).css('padding-bottom',item_height);
    });
    $('a.authors-item').each(function() {
        var item_height = $(this).find('.authors-item-info').outerHeight();
        $(this).css('padding-bottom',item_height);
    });
};

$.fn.scr1200 = function() {
    $(".container.grid").shapeshift({
        gutterX: 93,
        gutterY: 130,
        paddingX: 0,
        enableDrag: false
    });
};

$.fn.scr767 = function() {
    $(".container.grid").shapeshift({
        gutterX: 52,
        gutterY: 130,
        paddingX: 0,
        enableDrag: false
    });
};

$.fn.scr450 = function() {
    $(".container.grid").shapeshift({
        gutterX: 30,
        gutterY: 130,
        paddingX: 0,
        enableDrag: false
    });
};

$.fn.AUTscr1200 = function() {
    $(".container.autoriai").shapeshift({
        gutterX: 56,
        gutterY: 56,
        paddingX: 0,
        enableDrag: false,
        selector: ".author-item"
    });
};

$.fn.AUTscr767 = function() {
    $(".container.autoriai").shapeshift({
        gutterX: 60,
        gutterY: 60,
        paddingX: 0,
        enableDrag: false,
        selector: ".author-item"
    });
};

$.fn.AUTscr450 = function() {
    $(".container.autoriai").shapeshift({
        gutterX: 15,
        gutterY: 50,
        paddingX: 0,
        enableDrag: false,
        selector: ".author-item"
    });
};

$(window).on('resize', function(){
    $.fn.items_hover();

    if (parseInt($(window).width()) < 450) {
        $.fn.scr450();
        $.fn.AUTscr450();
    }

    if (parseInt($(window).width()) > 450) {
        $.fn.scr450();
        $.fn.AUTscr450();
    }

    if (parseInt($(window).width()) > 767) {
        $.fn.scr767();
        $.fn.AUTscr767();
    }

    if (parseInt($(window).width()) > 1200) {
        $.fn.scr1200();
        $.fn.AUTscr1200();
    }
});


$(document).ready(function() {

    $(".thisyear").text( (new Date).getFullYear() );

    $.fn.items_hover();

    if (parseInt($(window).width()) < 450) {
        $.fn.scr450();
        $.fn.AUTscr450();
    }

    if (parseInt($(window).width()) > 450) {
        $.fn.scr450();
        $.fn.AUTscr450();
    }

    if (parseInt($(window).width()) > 767) {
        $.fn.scr767();
        $.fn.AUTscr767();
    }

    if (parseInt($(window).width()) > 1200) {
        $.fn.scr1200();
        $.fn.AUTscr1200();
    }


    //$(".container.grid").shapeshift({
    //    gutterX: 30,
    //    gutterY: 130,
    //    paddingX: 0,
    //    enableDrag: false
    //});
    //
    //$(".container.autoriai").shapeshift({
    //    gutterX: 52,
    //    gutterY: 105,
    //    paddingX: 0,
    //    enableDrag: false,
    //    selector: ".author-item"
    //});

    $(".container.autorius").shapeshift({
        gutterX: 105,
        gutterY: 105,
        paddingX: 0,
        enableDrag: false,
        selector: ".authors-item"
    });

    $("#search").click(function() {
        $(".search-bar").slideToggle();
        //$("#mainsrch").select();
    });

    $('.exitsrch').click(function() {
        $(".search-bar").slideToggle();
    });

    function watchTextbox() {
        var txtInput = $('#mainsrch');
        var lastValue = txtInput.data('lastValue');
        var currentValue = txtInput.val();
        if (lastValue != currentValue) {
            console.log('Value changed from ' + lastValue + ' to ' + currentValue);
            txtInput.data('lastValue', currentValue);

            $( ".search-bar" ).append( "<div><h2>Autoriai</h2><a href='#'>Vienas autorius</a><a href='#'>Antras autorius ilgesne pavarde</a><a href='#'>Trečias autorius</a></div>");
            $( ".search-bar" ).append( "<div><h2>Kūriniai</h2><a href='#'>Vienas kūrinys</a><a href='#'>Antras kūrinys ilgesniu pavadinimu</a><a href='#'>Trečias kūrinys su labai ilgu pavadinimu jei tokių būna ;)</a></div>");
        }
    }

    $('#mainsrch').data('lastValue', $('#mainsrch').val());
    $('#mainsrch').bind('keypress set', null, watchTextbox);
    setInterval(watchTextbox, 100);


    $(".hambur").click(function() {
        if ($(".filter").hasClass("expanded")) {
            $(".filter").toggleClass("expanded");
            $(".meniu").toggleClass("expanded");
        }
        else {
            $(".meniu").toggleClass("expanded");
        }
    });

    $(".filter-call").click(function() {
        if ($(".meniu").hasClass("expanded")) {
            $(".meniu").toggleClass("expanded");
            $(".filter").toggleClass("expanded");
        }
        else {
            $(".filter").toggleClass("expanded");
        }
    });

    $(".read-more").click(function() {
        $(".authors-info-expanded").slideToggle( "slow" );

        var text = $('.read-more').text();
        if (text == 'Skaityti daugiau'){
            $('.read-more').text('Skaityti mažiau');
        }
        else {
            $('.read-more').text('Skaityti daugiau');
        }
    });


    $( "span#zooming" ).mouseover(function() {
        $(".tags").show();
    }).mouseleave(function() {
        $(".tags").hide();
    });

    $( ".tags>.tag" ).mouseover(function() {
        $(this).find("p").show();
        $(".tags").show();
    }).mouseleave(function() {
        $(this).find("p").hide();
    });

    $('span#zooming').zoom({
        //url: 'photo-big.jpg',
        on: 'grab',
        onZoomIn: function() { $(".tags").hide(); $(".zoom").hide(); },
        onZoomOut: function() { $(".tags").show(); $(".zoom").show(); }
    });

    $(".tag-icon").click(function() {
        $(".tags").toggleClass('forcenone');
        $(this).toggleClass('active');
    });

    $(".tekstinis").click(function() {
        $('.itemai').toggle();
    });

});